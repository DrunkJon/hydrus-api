#!/usr/bin/env python3

import argparse
import collections
import os

import hydrus
import hydrus.utils

REQUIRED_PERMISSIONS = [hydrus.Permission.ImportFiles, hydrus.Permission.AddTags]
ERROR_EXIT_CODE = 1

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("api_key")
argument_parser.add_argument("paths", nargs="+")
argument_parser.add_argument("--tag", "-t", action="append", dest="tags", default=[])
argument_parser.add_argument("--service", "-s", action="append", dest="services", default=["my tags"])
argument_parser.add_argument("--no-recursive", "-n", action="store_false", dest="recursive")
argument_parser.add_argument("--no-read-metadata", "-m", action="store_false", dest="read_metadata")
argument_parser.add_argument("--api-url", "-a", default=hydrus.DEFAULT_API_URL)


def yield_paths(path, predicate=lambda path: path, recursive=True):
    for path, directories, file_names in os.walk(path, followlinks=True):
        if predicate(path):
            yield path

        for file_name in file_names:
            file_path = os.path.join(path, file_name)
            if predicate(file_path):
                yield file_path

        if not recursive:
            break


def valid_file_path(path):
    return os.path.isfile(path) or os.path.islink(path) and os.path.isfile(os.path.realpath(path))


def import_path(client, path, tags=(), read_metadata=True, recursive=True, services=("my tags",)):
    default_tags = tags
    tag_sets = collections.defaultdict(set)

    for path in yield_paths(path, valid_file_path, recursive):
        # Attempt to import all files (don't check MIME type), except Hydrus metadata
        # files
        if os.path.splitext(path)[1].lower() == ".txt":
            continue

        directory_path, filename = os.path.split(path)
        metadata_path = os.path.join(directory_path, filename + ".txt")
        tags = (
            hydrus.utils.read_hydrus_metadata_file(metadata_path)
            if read_metadata and valid_file_path(metadata_path)
            else set()
        )
        tags.update(default_tags)
        tag_sets[tuple(sorted(tags))].add(os.path.realpath(path))

    for tags, paths in tag_sets.items():
        client.add_and_tag_files(paths, tags, services)


def main(arguments):
    client = hydrus.Client(arguments.api_key, arguments.api_url)
    if not hydrus.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ERROR_EXIT_CODE

    for path in arguments.paths:
        print("Importing {}...".format(path))
        import_path(
            client,
            path,
            arguments.tags,
            arguments.read_metadata,
            arguments.recursive,
            arguments.services,
        )


if __name__ == "__main__":
    arguments = argument_parser.parse_args()
    argument_parser.exit(main(arguments))
