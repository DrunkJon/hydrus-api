#!/usr/bin/env python3

import argparse
import re

import hydrus
import hydrus.utils

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("api_key")
argument_parser.add_argument("regex")
argument_parser.add_argument("tags", nargs="+")
argument_parser.add_argument("--chunk-size", "-c", type=int, default=100)
argument_parser.add_argument("--output", "-o", default="urls.txt")
argument_parser.add_argument("--api-url", "-a", default=hydrus.DEFAULT_API_URL)

REQUIRED_PERMISSIONS = [hydrus.Permission.SearchFiles]
ERROR_EXIT_CODE = 1
ENCODING = "UTF-8"


def main(arguments):
    client = hydrus.Client(arguments.api_key, arguments.api_url)
    if not hydrus.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ERROR_EXIT_CODE

    regex = re.compile(arguments.regex)
    output = open(arguments.output, "w", encoding=ENCODING)

    file_ids = client.search_files(arguments.tags)
    for file_ids in hydrus.utils.yield_chunks(file_ids, arguments.chunk_size):
        metadata = client.file_metadata(file_ids=file_ids)
        for metadatum in metadata:
            for url in metadatum["known_urls"]:
                if regex.match(url):
                    print(url)
                    output.write(url + "\n")


if __name__ == "__main__":
    arguments = argument_parser.parse_args()
    argument_parser.exit(main(arguments))
