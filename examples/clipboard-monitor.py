#!/usr/bin/env python3

import argparse
import subprocess
import time

import pyperclip

import hydrus
import hydrus.utils

REQUIRED_PERMISSIONS = [hydrus.Permission.ImportURLs, hydrus.Permission.ManagePages]
ERROR_EXIT_CODE = 1

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("api_key")
argument_parser.add_argument("--interval", "-i", type=float, default=0.1)
argument_parser.add_argument("--api-url", "-a", default=hydrus.DEFAULT_API_URL)


def dmenu(options):
    process = subprocess.run(
        ["dmenu"],
        input="\n".join(options),
        capture_output=True,
        universal_newlines=True,
    )
    return process.stdout.strip()


def get_downloader_pages(client):
    return [
        page["name"]
        for page in client.get_pages(flatten=True)
        if page["page_type"] in {hydrus.PageType.ThreadWatcher, hydrus.PageType.URLDownloader}
    ]


def main(arguments):
    client = hydrus.Client(arguments.api_key, arguments.api_url)
    if not hydrus.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ERROR_EXIT_CODE

    old_contents = pyperclip.paste()
    while True:
        contents = pyperclip.paste()
        if contents == old_contents:
            time.sleep(arguments.interval)
            continue

        old_contents = contents

        try:
            url_info = client.get_url_info(contents)
        except hydrus.ServerError:
            print("Unrecognized URL:", repr(contents))
            time.sleep(arguments.interval)
            continue

        pages = get_downloader_pages(client)
        candidate_count = len(pages)
        if candidate_count > 1:
            page_name = dmenu(pages)
            if not page_name:
                continue
        elif candidate_count == 1:
            page_name = arguments.pages[0]
        else:
            page_name = None

        normalized_url = url_info["normalised_url"]
        client.add_url(normalized_url, page_name=page_name)
        print("Added", normalized_url, "to page", repr(page_name))
        time.sleep(arguments.interval)


if __name__ == "__main__":
    arguments = argument_parser.parse_args()
    try:
        argument_parser.exit(main(arguments))
    except KeyboardInterrupt:
        pass
