#!/usr/bin/env python3

import argparse
import os

import hydrus
import hydrus.utils

REQUIRED_PERMISSIONS = [hydrus.Permission.SearchFiles]
ERROR_EXIT_CODE = 1

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("api_key")
argument_parser.add_argument("tags", nargs="+")
argument_parser.add_argument("--or-search", "-o", action="store_true")
argument_parser.add_argument("--inbox-only", "-i", action="store_true")
argument_parser.add_argument("--archive-only", "-A", action="store_true")
argument_parser.add_argument("--service", "-s", dest="services", action="store", default=["my tags"])
argument_parser.add_argument("--write-metadata", "-m", action="store_true")
argument_parser.add_argument("--limit", "-l", type=int, default=None)
argument_parser.add_argument("--destination", "-d", default=os.getcwd())
argument_parser.add_argument("--chunk-size", "-c", type=int, default=100)
argument_parser.add_argument("--api-url", "-a", default=hydrus.DEFAULT_API_URL)


def main(arguments):
    client = hydrus.Client(arguments.api_key, arguments.api_url)
    if not hydrus.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ERROR_EXIT_CODE

    all_file_ids = set()
    if arguments.or_search:
        for tag in arguments.tags:
            all_file_ids.update(client.search_files([tag], arguments.inbox_only, arguments.archive_only))
    else:
        all_file_ids.update(client.search_files(arguments.tags, arguments.inbox_only, arguments.archive_only))

    exported = 0
    print("Exporting {} files to {}... ".format(arguments.limit or len(all_file_ids), arguments.destination))
    for file_ids in hydrus.utils.yield_chunks(list(all_file_ids), arguments.chunk_size):
        metadata = client.file_metadata(file_ids=file_ids)
        for metadatum in metadata:
            if arguments.limit is not None and exported >= arguments.limit:
                return

            mime_type = metadatum["mime"]
            extension = "." + mime_type.rsplit("/", 1)[1] if "/" in mime_type else ""
            path = os.path.join(arguments.destination, metadatum["hash"] + extension)
            with open(path, "wb") as file:
                file.write(client.get_file(file_id=metadatum["file_id"]))

            if arguments.write_metadata:
                tags = set()
                services_to_statuses = metadatum["service_names_to_statuses_to_tags"]
                for service_name in set(arguments.services):
                    service = services_to_statuses.get(service_name)
                    if service:
                        current_tags = service.get(hydrus.TagStatus.Current)
                        if current_tags:
                            tags.update(current_tags)

                with open(path + ".txt", "w", encoding=hydrus.utils.HYDRUS_ENCODING) as file:
                    file.write("\n".join(sorted(tags)))

            exported += 1


if __name__ == "__main__":
    arguments = argument_parser.parse_args()
    argument_parser.exit(main(arguments))
